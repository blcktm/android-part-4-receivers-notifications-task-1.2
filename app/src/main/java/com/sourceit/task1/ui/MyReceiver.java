package com.sourceit.task1.ui;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.support.v4.content.LocalBroadcastManager;

import com.sourceit.task1.App;
import com.sourceit.task1.R;
import com.sourceit.task1.utils.L;

import static com.sourceit.task1.ui.States.AIR;
import static com.sourceit.task1.ui.States.BLUETOOTH;
import static com.sourceit.task1.ui.States.GPS;
import static com.sourceit.task1.ui.States.WIFI;
import static com.sourceit.task1.ui.States.checkAllSensors;
import static com.sourceit.task1.ui.States.delNotification;
import static com.sourceit.task1.ui.States.sensorStates;

/**
 * Created by ${blcktm} on 12.02.2016.
 */
public class MyReceiver extends BroadcastReceiver {

    private static Resources res = App.getApp().getResources();

    public final String ANDROID_INTENT_ACTION_AIRPLANE_MODE = res.getString(R.string.action_airplane);
    public final String ANDROID_BLUETOOTH_ADAPTER_ACTION_STATE_CHANGED = res.getString(R.string.action_bluetooth);
    public static final String ANDROID_LOCATION_PROVIDERS_CHANGED = res.getString(R.string.action_location);
    public static final String ANDROID_NET_WIFI_WIFI_STATE_CHANGED = res.getString(R.string.action_wifi);

    final String LOCAL_WIFI = res.getString(R.string.state_wifi);
    final String LOCAL_BLUETOOTH = res.getString(R.string.state_bluetooth);
    final String LOCAL_GPS = res.getString(R.string.state_gps);
    final String LOCAL_AIR = res.getString(R.string.state_air);

    private Intent localIntent;

    @Override
    public void onReceive(Context context, Intent intent) {
        L.d("onReceive " + intent.getAction());

        if (intent.getAction().equals(ANDROID_INTENT_ACTION_AIRPLANE_MODE)) {
            if (!intent.getBooleanExtra("state", false)) {
                L.d("onReceive airplane false");
                delNotification();
                sensorStates ^= AIR;
            } else {
                L.d("onReceive airplane true");
                sensorStates |= AIR;
                checkAllSensors();
            }

            localIntent = new Intent(LOCAL_AIR);
            LocalBroadcastManager.getInstance(context).sendBroadcast(localIntent);

        } else if (intent.getAction().equals(ANDROID_NET_WIFI_WIFI_STATE_CHANGED)) {
            L.d("Wi-Fi state ");
            if (intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, 0) == WifiManager.WIFI_STATE_DISABLED) {
                if ((sensorStates & WIFI) == WIFI) {
                    L.d("wi-fi false");
                    sensorStates ^= WIFI;
                    checkAllSensors();
                }
            } else if (intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, 0) == WifiManager.WIFI_STATE_ENABLED) {
                L.d("wi-fi true");
                sensorStates |= WIFI;
                checkAllSensors();
            }

            localIntent = new Intent(LOCAL_WIFI);
            LocalBroadcastManager.getInstance(context).sendBroadcast(localIntent);

        } else if (intent.getAction().equals(ANDROID_BLUETOOTH_ADAPTER_ACTION_STATE_CHANGED)) {
            L.d("bluetooth");
            if (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, 0) == BluetoothAdapter.STATE_ON) {
                L.d("bluetooth on");
                sensorStates |= BLUETOOTH;
                checkAllSensors();
            } else if (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, 0) == BluetoothAdapter.STATE_OFF) {
                L.d("bluetooth off");
                sensorStates ^= BLUETOOTH;
                checkAllSensors();
            }

            localIntent = new Intent(LOCAL_BLUETOOTH);
            LocalBroadcastManager.getInstance(context).sendBroadcast(localIntent);

        } else if (intent.getAction().equals(ANDROID_LOCATION_PROVIDERS_CHANGED)) {
            LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            L.d("gps" + manager.isProviderEnabled(LocationManager.GPS_PROVIDER));
            if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                L.d("gps on");
                sensorStates |= GPS;
                checkAllSensors();
            } else if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                if ((sensorStates & GPS) == GPS) {
                    sensorStates ^= GPS;
                    L.d("sensorStates ^= GPS");
                    checkAllSensors();
                }
            }

            localIntent = new Intent(LOCAL_GPS);
            LocalBroadcastManager.getInstance(context).sendBroadcast(localIntent);
        }
    }
}


