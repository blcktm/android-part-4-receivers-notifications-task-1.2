package com.sourceit.task1.ui;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.sourceit.task1.App;
import com.sourceit.task1.R;
import com.sourceit.task1.model.BroadcasterModel;
import com.sourceit.task1.utils.L;

import java.util.ArrayList;

import static com.sourceit.task1.ui.States.AIR;
import static com.sourceit.task1.ui.States.BLUETOOTH;
import static com.sourceit.task1.ui.States.GPS;
import static com.sourceit.task1.ui.States.WIFI;
import static com.sourceit.task1.ui.States.checkAllSensors;
import static com.sourceit.task1.ui.States.checkStateAir;
import static com.sourceit.task1.ui.States.checkStateBluetooth;
import static com.sourceit.task1.ui.States.checkStateGps;
import static com.sourceit.task1.ui.States.checkStateWifi;
import static com.sourceit.task1.ui.States.sensorStates;

public class MainActivity extends AppCompatActivity {

    private static Resources res = App.getApp().getResources();

    public static final String ANDROID_INTENT_ACTION_AIRPLANE_MODE = res.getString(R.string.state_air);
    public static final String ANDROID_BLUETOOTH_ADAPTER_ACTION_STATE_CHANGED = res.getString(R.string.state_bluetooth);
    public static final String ANDROID_LOCATION_PROVIDERS_CHANGED = res.getString(R.string.state_gps);
    public static final String ANDROID_NET_WIFI_WIFI_STATE_CHANGED = res.getString(R.string.state_wifi);

    private RecyclerView broadcasters;
    private int ON_AIR = 1;

    BroadcasterModel broadcasterModel_wifi;
    BroadcasterModel broadcasterModel_bluetooth;
    BroadcasterModel broadcasterModel_gps;
    BroadcasterModel broadcasterModel_airmode;

    private ArrayList<BroadcasterModel> broadcasterModels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        L.d("start activity");

        broadcasters = (RecyclerView) findViewById(R.id.broadcasters_list);
        broadcasterModels = new ArrayList<>();

        checkGps();
        checkBluetooth();
        checkWifi();
        checkAir();

        broadcasterModel_wifi = new BroadcasterModel(res.getString(R.string.state_wifi));
        broadcasterModel_bluetooth = new BroadcasterModel(res.getString(R.string.state_bluetooth));
        broadcasterModel_gps = new BroadcasterModel(res.getString(R.string.state_gps));
        broadcasterModel_airmode = new BroadcasterModel(res.getString(R.string.state_air));

        broadcasterModels.add(broadcasterModel_wifi);
        broadcasterModels.add(broadcasterModel_bluetooth);
        broadcasterModels.add(broadcasterModel_gps);
        broadcasterModels.add(broadcasterModel_airmode);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        broadcasters.setLayoutManager(layoutManager);
        L.d("broadcasterModels.size :" + broadcasterModels.size());
        broadcasters.setAdapter(new MyAdapter(broadcasterModels));
    }

    @Override
    protected void onResume() {
        super.onResume();
        L.d("resume");

        setStates();

        IntentFilter iff = new IntentFilter();
        iff.addAction(ANDROID_INTENT_ACTION_AIRPLANE_MODE);
        iff.addAction(ANDROID_NET_WIFI_WIFI_STATE_CHANGED);
        iff.addAction(ANDROID_LOCATION_PROVIDERS_CHANGED);
        iff.addAction(ANDROID_BLUETOOTH_ADAPTER_ACTION_STATE_CHANGED);
        LocalBroadcastManager.getInstance(this).registerReceiver(localReceiver, iff);
    }

    @Override
    protected void onPause() {
        super.onPause();
        L.d("paused");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(localReceiver);
    }

    private BroadcastReceiver localReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            L.d("local receive");
            if (intent.getAction().equals(ANDROID_INTENT_ACTION_AIRPLANE_MODE)) {
                L.d("local air mode");
                L.d("state AIR - " + checkStateAir());
                broadcasterModel_airmode.setState(checkStateAir());
                broadcasters.getAdapter().notifyDataSetChanged();
            } else if (intent.getAction().equals(ANDROID_NET_WIFI_WIFI_STATE_CHANGED)) {
                L.d("local wifi mode");
                broadcasterModel_wifi.setState(checkStateWifi());
                broadcasters.getAdapter().notifyDataSetChanged();
            } else if (intent.getAction().equals(ANDROID_BLUETOOTH_ADAPTER_ACTION_STATE_CHANGED)) {
                L.d("local bluetooth mode");
                broadcasterModel_bluetooth.setState(checkStateBluetooth());
                broadcasters.getAdapter().notifyDataSetChanged();
            } else if (intent.getAction().equals(ANDROID_LOCATION_PROVIDERS_CHANGED)) {
                L.d("local gps mode");
                broadcasterModel_gps.setState(checkStateGps());
                broadcasters.getAdapter().notifyDataSetChanged();
            }
        }
    };

    void checkGps() {
        LocationManager managerGps = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        L.d("gps :" + managerGps.isProviderEnabled(LocationManager.GPS_PROVIDER));
        if (managerGps.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            sensorStates |= GPS;
            L.d("GPS on");
        }
    }

    void checkBluetooth() {
        BluetoothAdapter bluetooth = BluetoothAdapter.getDefaultAdapter();
        if (bluetooth != null) {
            L.d("bluetooth : " + bluetooth.isEnabled());
            if (bluetooth.isEnabled()) {
                sensorStates |= BLUETOOTH;
                L.d("Bluetooth on");
            }
        }
    }

    void checkWifi() {
        WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        L.d("wifi: " + wifi.isWifiEnabled());
        if (wifi.isWifiEnabled()) {
            sensorStates |= WIFI;
            L.d("Wi-Fi on");
        }
    }

    void checkAir() {
        if (android.provider.Settings.System.getInt(
                getContentResolver(),
                android.provider.Settings.Global.AIRPLANE_MODE_ON, 0) == ON_AIR) {
            sensorStates |= AIR;
            L.d("AIR on");

            checkAllSensors();
        }
    }

    void setStates() {

        broadcasterModel_wifi.setState(checkStateWifi());
        broadcasterModel_bluetooth.setState(checkStateBluetooth());
        broadcasterModel_gps.setState(checkStateGps());
        broadcasterModel_airmode.setState(checkStateAir());

        broadcasters.getAdapter().notifyDataSetChanged();
    }
}