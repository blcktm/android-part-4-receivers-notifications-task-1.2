package com.sourceit.task1.ui;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.NotificationCompat;

import com.sourceit.task1.App;
import com.sourceit.task1.R;
import com.sourceit.task1.utils.L;

/**
 * Created by ${blcktm} on 13.02.2016.
 */
public class States {

    static Context context = App.getApp();

    static int sensorStates;

    final static int AIR = 1;
    final static int BLUETOOTH = 2;
    final static int GPS = 4;
    final static int WIFI = 8;


    static private final int ID = 1;
    static final long[] DOT = new long[200];

    static NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    static Intent notificationIntent = new Intent();
    static PendingIntent contentIntent = PendingIntent.getActivity(context,
            ID, notificationIntent,
            PendingIntent.FLAG_CANCEL_CURRENT);

    static void checkAllSensors() {
        if ((sensorStates & AIR) == AIR) {
            if (Integer.bitCount(sensorStates >> 1) == Integer.bitCount(WIFI | BLUETOOTH | GPS)) {

                L.d("Note 3");
                NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
                builder.setContentIntent(contentIntent)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setTicker(context.getString(R.string.note1))
                        .setContentText(context.getString(R.string.note1))
                        .setLights(Color.BLUE, 500, 500);
                Notification n = builder.build();
                nm.notify(ID, n);
            } else if (Integer.bitCount(sensorStates >> 1) == Integer.bitCount(WIFI | BLUETOOTH)) {

                L.d("Note 2");
                NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
                builder.setContentIntent(contentIntent)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setTicker(context.getString(R.string.note2))
                        .setContentText(context.getString(R.string.note2))
                        .setVibrate(DOT);
                Notification n = builder.build();
                nm.notify(ID, n);
            } else if (Integer.bitCount(sensorStates >> 1) == Integer.bitCount(WIFI)) {

                L.d("Note 1");
                NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
                builder.setContentIntent(contentIntent)
                        .setSmallIcon(R.drawable.ic_change_history_black_24dp)
                        .setTicker(context.getString(R.string.note4))
                        .setContentText(context.getString(R.string.note4));
                Notification n = builder.build();
                nm.notify(ID, n);
            } else {
                delNotification();
            }
        }
    }

    static void delNotification() {
        nm.cancel(ID);
    }

    static boolean checkStateWifi() {
        return (sensorStates & WIFI) == WIFI;
    }

    static boolean checkStateBluetooth() {
        return (sensorStates & BLUETOOTH) == BLUETOOTH;
    }

    static boolean checkStateGps() {
        return (sensorStates & GPS) == GPS;
    }

    static boolean checkStateAir() {
        return (sensorStates & AIR) == AIR;
    }
}
